# Bastion deployment script

Script used on our GCP bastion instances to handle GitLab Runner deployments
inside of screen.

## Author

Tomasz Maczukin, 2021, GitLab

## License

MIT

